<?php

namespace Fuel\Migrations;

class Create_newsevents
{
	public function up()
	{
		\DBUtil::create_table('newsevents', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'date' => array('constraint' => 11, 'type' => 'int'),
			'shortdescription' => array('constraint' => 255, 'type' => 'varchar'),
			'news' => array('type' => 'text'),
			'image' => array('constraint' => 255, 'type' => 'varchar'),
			'status' => array('constraint' => 2, 'type' => 'tinyint'),
			'institudeId' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('newsevents');
	}
}