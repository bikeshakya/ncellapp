<?php

namespace Fuel\Migrations;

class Create_certificates
{
	public function up()
	{
		\DBUtil::create_table('certificates', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'batch' => array('constraint' => 11, 'type' => 'int'),
			'resultyear' => array('constraint' => 11, 'type' => 'int'),
			'examyear' => array('constraint' => 11, 'type' => 'int'),
			'regId' => array('constraint' => 11, 'type' => 'int'),
			'status' => array('constraint' => 2, 'type' => 'tinyint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('certificates');
	}
}