<?php

namespace Fuel\Migrations;

class Create_uvnewsevents
{
	public function up()
	{
		\DBUtil::create_table('uvnewsevents', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'date' => array('constraint' => 11, 'type' => 'int'),
			'description' => array('type' => 'text'),
			'uvid' => array('constraint' => 11, 'type' => 'int'),
			'status' => array('constraint' => 2, 'type' => 'tinyint'),
			'batch' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('uvnewsevents');
	}
}