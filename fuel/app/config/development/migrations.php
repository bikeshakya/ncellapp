<?php
return array(
  'version' => 
  array(
    'app' => 
    array(
      'default' => 
      array(
        0 => '001_create_users',
        1 => '002_create_userroles',
        2 => '003_create_institutes',
        3 => '004_create_newsevents',
        4 => '005_create_reqfeeds',
        5 => '006_create_certificates',
        6 => '007_create_uvnewsevents',
      ),
    ),
    'module' => 
    array(
    ),
    'package' => 
    array(
    ),
  ),
  'folder' => 'migrations/',
  'table' => 'migration',
);
