<?php
class Controller_Reqfeed extends Controller_Template
{

	public function action_index()
	{
		$data['reqfeeds'] = Model_Reqfeed::find('all');
		$this->template->title = "Reqfeeds";
		$this->template->content = View::forge('reqfeed/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('reqfeed');

		if ( ! $data['reqfeed'] = Model_Reqfeed::find($id))
		{
			Session::set_flash('error', 'Could not find reqfeed #'.$id);
			Response::redirect('reqfeed');
		}

		$this->template->title = "Reqfeed";
		$this->template->content = View::forge('reqfeed/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Reqfeed::validate('create');

			if ($val->run())
			{
				$reqfeed = Model_Reqfeed::forge(array(
					'userfromid' => Input::post('userfromid'),
					'usertoid' => Input::post('usertoid'),
					'message' => Input::post('message'),
					'status' => Input::post('status'),
				));

				if ($reqfeed and $reqfeed->save())
				{
					Session::set_flash('success', 'Added reqfeed #'.$reqfeed->id.'.');

					Response::redirect('reqfeed');
				}

				else
				{
					Session::set_flash('error', 'Could not save reqfeed.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Reqfeeds";
		$this->template->content = View::forge('reqfeed/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('reqfeed');

		if ( ! $reqfeed = Model_Reqfeed::find($id))
		{
			Session::set_flash('error', 'Could not find reqfeed #'.$id);
			Response::redirect('reqfeed');
		}

		$val = Model_Reqfeed::validate('edit');

		if ($val->run())
		{
			$reqfeed->userfromid = Input::post('userfromid');
			$reqfeed->usertoid = Input::post('usertoid');
			$reqfeed->message = Input::post('message');
			$reqfeed->status = Input::post('status');

			if ($reqfeed->save())
			{
				Session::set_flash('success', 'Updated reqfeed #' . $id);

				Response::redirect('reqfeed');
			}

			else
			{
				Session::set_flash('error', 'Could not update reqfeed #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$reqfeed->userfromid = $val->validated('userfromid');
				$reqfeed->usertoid = $val->validated('usertoid');
				$reqfeed->message = $val->validated('message');
				$reqfeed->status = $val->validated('status');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('reqfeed', $reqfeed, false);
		}

		$this->template->title = "Reqfeeds";
		$this->template->content = View::forge('reqfeed/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('reqfeed');

		if ($reqfeed = Model_Reqfeed::find($id))
		{
			$reqfeed->delete();

			Session::set_flash('success', 'Deleted reqfeed #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete reqfeed #'.$id);
		}

		Response::redirect('reqfeed');

	}

}
