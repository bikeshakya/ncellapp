<?php
class Controller_Uvnewsevent extends Controller_Template
{

	public function action_index()
	{
		$data['uvnewsevents'] = Model_Uvnewsevent::find('all');
		$this->template->title = "Uvnewsevents";
		$this->template->content = View::forge('uvnewsevent/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('uvnewsevent');

		if ( ! $data['uvnewsevent'] = Model_Uvnewsevent::find($id))
		{
			Session::set_flash('error', 'Could not find uvnewsevent #'.$id);
			Response::redirect('uvnewsevent');
		}

		$this->template->title = "Uvnewsevent";
		$this->template->content = View::forge('uvnewsevent/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Uvnewsevent::validate('create');

			if ($val->run())
			{
				$uvnewsevent = Model_Uvnewsevent::forge(array(
					'date' => Input::post('date'),
					'description' => Input::post('description'),
					'uvid' => Input::post('uvid'),
					'status' => Input::post('status'),
					'batch' => Input::post('batch'),
				));

				if ($uvnewsevent and $uvnewsevent->save())
				{
					Session::set_flash('success', 'Added uvnewsevent #'.$uvnewsevent->id.'.');

					Response::redirect('uvnewsevent');
				}

				else
				{
					Session::set_flash('error', 'Could not save uvnewsevent.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Uvnewsevents";
		$this->template->content = View::forge('uvnewsevent/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('uvnewsevent');

		if ( ! $uvnewsevent = Model_Uvnewsevent::find($id))
		{
			Session::set_flash('error', 'Could not find uvnewsevent #'.$id);
			Response::redirect('uvnewsevent');
		}

		$val = Model_Uvnewsevent::validate('edit');

		if ($val->run())
		{
			$uvnewsevent->date = Input::post('date');
			$uvnewsevent->description = Input::post('description');
			$uvnewsevent->uvid = Input::post('uvid');
			$uvnewsevent->status = Input::post('status');
			$uvnewsevent->batch = Input::post('batch');

			if ($uvnewsevent->save())
			{
				Session::set_flash('success', 'Updated uvnewsevent #' . $id);

				Response::redirect('uvnewsevent');
			}

			else
			{
				Session::set_flash('error', 'Could not update uvnewsevent #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$uvnewsevent->date = $val->validated('date');
				$uvnewsevent->description = $val->validated('description');
				$uvnewsevent->uvid = $val->validated('uvid');
				$uvnewsevent->status = $val->validated('status');
				$uvnewsevent->batch = $val->validated('batch');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('uvnewsevent', $uvnewsevent, false);
		}

		$this->template->title = "Uvnewsevents";
		$this->template->content = View::forge('uvnewsevent/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('uvnewsevent');

		if ($uvnewsevent = Model_Uvnewsevent::find($id))
		{
			$uvnewsevent->delete();

			Session::set_flash('success', 'Deleted uvnewsevent #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete uvnewsevent #'.$id);
		}

		Response::redirect('uvnewsevent');

	}

}
