<?php
class Controller_Userrole extends Controller_Template
{

	public function action_index()
	{
		$data['userroles'] = Model_Userrole::find('all');
		$this->template->title = "Userroles";
		$this->template->content = View::forge('userrole/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('userrole');

		if ( ! $data['userrole'] = Model_Userrole::find($id))
		{
			Session::set_flash('error', 'Could not find userrole #'.$id);
			Response::redirect('userrole');
		}

		$this->template->title = "Userrole";
		$this->template->content = View::forge('userrole/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Userrole::validate('create');

			if ($val->run())
			{
				$userrole = Model_Userrole::forge(array(
					'rolename' => Input::post('rolename'),
				));

				if ($userrole and $userrole->save())
				{
					Session::set_flash('success', 'Added userrole #'.$userrole->id.'.');

					Response::redirect('userrole');
				}

				else
				{
					Session::set_flash('error', 'Could not save userrole.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Userroles";
		$this->template->content = View::forge('userrole/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('userrole');

		if ( ! $userrole = Model_Userrole::find($id))
		{
			Session::set_flash('error', 'Could not find userrole #'.$id);
			Response::redirect('userrole');
		}

		$val = Model_Userrole::validate('edit');

		if ($val->run())
		{
			$userrole->rolename = Input::post('rolename');

			if ($userrole->save())
			{
				Session::set_flash('success', 'Updated userrole #' . $id);

				Response::redirect('userrole');
			}

			else
			{
				Session::set_flash('error', 'Could not update userrole #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$userrole->rolename = $val->validated('rolename');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('userrole', $userrole, false);
		}

		$this->template->title = "Userroles";
		$this->template->content = View::forge('userrole/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('userrole');

		if ($userrole = Model_Userrole::find($id))
		{
			$userrole->delete();

			Session::set_flash('success', 'Deleted userrole #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete userrole #'.$id);
		}

		Response::redirect('userrole');

	}

}
