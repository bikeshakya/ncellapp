<?php
class Controller_Newsevent extends Controller_Template
{

	public function action_index()
	{
		$data['newsevents'] = Model_Newsevent::find('all');
		$this->template->title = "Newsevents";
		$this->template->content = View::forge('newsevent/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('newsevent');

		if ( ! $data['newsevent'] = Model_Newsevent::find($id))
		{
			Session::set_flash('error', 'Could not find newsevent #'.$id);
			Response::redirect('newsevent');
		}

		$this->template->title = "Newsevent";
		$this->template->content = View::forge('newsevent/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Newsevent::validate('create');

			if ($val->run())
			{
				$newsevent = Model_Newsevent::forge(array(
					'date' => Input::post('date'),
					'shortdescription' => Input::post('shortdescription'),
					'news' => Input::post('news'),
					'image' => Input::post('image'),
					'status' => Input::post('status'),
					'institudeid' => Input::post('institudeid'),
				));

				if ($newsevent and $newsevent->save())
				{
					Session::set_flash('success', 'Added newsevent #'.$newsevent->id.'.');

					Response::redirect('newsevent');
				}

				else
				{
					Session::set_flash('error', 'Could not save newsevent.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Newsevents";
		$this->template->content = View::forge('newsevent/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('newsevent');

		if ( ! $newsevent = Model_Newsevent::find($id))
		{
			Session::set_flash('error', 'Could not find newsevent #'.$id);
			Response::redirect('newsevent');
		}

		$val = Model_Newsevent::validate('edit');

		if ($val->run())
		{
			$newsevent->date = Input::post('date');
			$newsevent->shortdescription = Input::post('shortdescription');
			$newsevent->news = Input::post('news');
			$newsevent->image = Input::post('image');
			$newsevent->status = Input::post('status');
			$newsevent->institudeid = Input::post('institudeid');

			if ($newsevent->save())
			{
				Session::set_flash('success', 'Updated newsevent #' . $id);

				Response::redirect('newsevent');
			}

			else
			{
				Session::set_flash('error', 'Could not update newsevent #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$newsevent->date = $val->validated('date');
				$newsevent->shortdescription = $val->validated('shortdescription');
				$newsevent->news = $val->validated('news');
				$newsevent->image = $val->validated('image');
				$newsevent->status = $val->validated('status');
				$newsevent->institudeid = $val->validated('institudeid');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('newsevent', $newsevent, false);
		}

		$this->template->title = "Newsevents";
		$this->template->content = View::forge('newsevent/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('newsevent');

		if ($newsevent = Model_Newsevent::find($id))
		{
			$newsevent->delete();

			Session::set_flash('success', 'Deleted newsevent #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete newsevent #'.$id);
		}

		Response::redirect('newsevent');

	}

}
