<?php
class Controller_Institute extends Controller_Template
{

	public function action_index()
	{
		$data['institutes'] = Model_Institute::find('all');
		$this->template->title = "Institutes";
		$this->template->content = View::forge('institute/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('institute');

		if ( ! $data['institute'] = Model_Institute::find($id))
		{
			Session::set_flash('error', 'Could not find institute #'.$id);
			Response::redirect('institute');
		}

		$this->template->title = "Institute";
		$this->template->content = View::forge('institute/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Institute::validate('create');

			if ($val->run())
			{
				$institute = Model_Institute::forge(array(
					'name' => Input::post('name'),
					'address' => Input::post('address'),
				));

				if ($institute and $institute->save())
				{
					Session::set_flash('success', 'Added institute #'.$institute->id.'.');

					Response::redirect('institute');
				}

				else
				{
					Session::set_flash('error', 'Could not save institute.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Institutes";
		$this->template->content = View::forge('institute/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('institute');

		if ( ! $institute = Model_Institute::find($id))
		{
			Session::set_flash('error', 'Could not find institute #'.$id);
			Response::redirect('institute');
		}

		$val = Model_Institute::validate('edit');

		if ($val->run())
		{
			$institute->name = Input::post('name');
			$institute->address = Input::post('address');

			if ($institute->save())
			{
				Session::set_flash('success', 'Updated institute #' . $id);

				Response::redirect('institute');
			}

			else
			{
				Session::set_flash('error', 'Could not update institute #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$institute->name = $val->validated('name');
				$institute->address = $val->validated('address');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('institute', $institute, false);
		}

		$this->template->title = "Institutes";
		$this->template->content = View::forge('institute/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('institute');

		if ($institute = Model_Institute::find($id))
		{
			$institute->delete();

			Session::set_flash('success', 'Deleted institute #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete institute #'.$id);
		}

		Response::redirect('institute');

	}

}
