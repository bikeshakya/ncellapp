<?php
class Controller_Certificate extends Controller_Template
{

	public function action_index()
	{
		$data['certificates'] = Model_Certificate::find('all');
		$this->template->title = "Certificates";
		$this->template->content = View::forge('certificate/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('certificate');

		if ( ! $data['certificate'] = Model_Certificate::find($id))
		{
			Session::set_flash('error', 'Could not find certificate #'.$id);
			Response::redirect('certificate');
		}

		$this->template->title = "Certificate";
		$this->template->content = View::forge('certificate/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Certificate::validate('create');

			if ($val->run())
			{
				$certificate = Model_Certificate::forge(array(
					'batch' => Input::post('batch'),
					'resultyear' => Input::post('resultyear'),
					'examyear' => Input::post('examyear'),
					'regid' => Input::post('regid'),
					'status' => Input::post('status'),
				));

				if ($certificate and $certificate->save())
				{
					Session::set_flash('success', 'Added certificate #'.$certificate->id.'.');

					Response::redirect('certificate');
				}

				else
				{
					Session::set_flash('error', 'Could not save certificate.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Certificates";
		$this->template->content = View::forge('certificate/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('certificate');

		if ( ! $certificate = Model_Certificate::find($id))
		{
			Session::set_flash('error', 'Could not find certificate #'.$id);
			Response::redirect('certificate');
		}

		$val = Model_Certificate::validate('edit');

		if ($val->run())
		{
			$certificate->batch = Input::post('batch');
			$certificate->resultyear = Input::post('resultyear');
			$certificate->examyear = Input::post('examyear');
			$certificate->regid = Input::post('regid');
			$certificate->status = Input::post('status');

			if ($certificate->save())
			{
				Session::set_flash('success', 'Updated certificate #' . $id);

				Response::redirect('certificate');
			}

			else
			{
				Session::set_flash('error', 'Could not update certificate #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$certificate->batch = $val->validated('batch');
				$certificate->resultyear = $val->validated('resultyear');
				$certificate->examyear = $val->validated('examyear');
				$certificate->regid = $val->validated('regid');
				$certificate->status = $val->validated('status');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('certificate', $certificate, false);
		}

		$this->template->title = "Certificates";
		$this->template->content = View::forge('certificate/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('certificate');

		if ($certificate = Model_Certificate::find($id))
		{
			$certificate->delete();

			Session::set_flash('success', 'Deleted certificate #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete certificate #'.$id);
		}

		Response::redirect('certificate');

	}

}
