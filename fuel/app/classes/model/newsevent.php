<?php
use Orm\Model;

class Model_Newsevent extends Model
{
	protected static $_properties = array(
		'id',
		'date',
		'shortdescription',
		'news',
		'image',
		'status',
		'institudeid',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('date', 'Date', 'required|valid_string[numeric]');
		$val->add_field('shortdescription', 'Shortdescription', 'required|max_length[255]');
		$val->add_field('news', 'News', 'required');
		$val->add_field('image', 'Image', 'required|max_length[255]');
		$val->add_field('status', 'Status', 'required');
		$val->add_field('institudeid', 'Institudeid', 'required|valid_string[numeric]');

		return $val;
	}

}
