<?php
use Orm\Model;

class Model_Certificate extends Model
{
	protected static $_properties = array(
		'id',
		'batch',
		'resultyear',
		'examyear',
		'regid',
		'status',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('batch', 'Batch', 'required|valid_string[numeric]');
		$val->add_field('resultyear', 'Resultyear', 'required|valid_string[numeric]');
		$val->add_field('examyear', 'Examyear', 'required|valid_string[numeric]');
		$val->add_field('regid', 'Regid', 'required|valid_string[numeric]');
		$val->add_field('status', 'Status', 'required');

		return $val;
	}

}
