<?php
use Orm\Model;

class Model_Uvnewsevent extends Model
{
	protected static $_properties = array(
		'id',
		'date',
		'description',
		'uvid',
		'status',
		'batch',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('date', 'Date', 'required|valid_string[numeric]');
		$val->add_field('description', 'Description', 'required');
		$val->add_field('uvid', 'Uvid', 'required|valid_string[numeric]');
		$val->add_field('status', 'Status', 'required');
		$val->add_field('batch', 'Batch', 'required|valid_string[numeric]');

		return $val;
	}

}
