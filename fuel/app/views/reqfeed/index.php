<h2>Listing <span class='muted'>Reqfeeds</span></h2>
<br>
<?php if ($reqfeeds): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Userfromid</th>
			<th>Usertoid</th>
			<th>Message</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($reqfeeds as $item): ?>		<tr>

			<td><?php echo $item->userfromid; ?></td>
			<td><?php echo $item->usertoid; ?></td>
			<td><?php echo $item->message; ?></td>
			<td><?php echo $item->status; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('reqfeed/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('reqfeed/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('reqfeed/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-small btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Reqfeeds.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('reqfeed/create', 'Add new Reqfeed', array('class' => 'btn btn-success')); ?>

</p>
