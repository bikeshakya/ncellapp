<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Userfromid', 'userfromid', array('class'=>'control-label')); ?>

				<?php echo Form::input('userfromid', Input::post('userfromid', isset($reqfeed) ? $reqfeed->userfromid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Userfromid')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Usertoid', 'usertoid', array('class'=>'control-label')); ?>

				<?php echo Form::input('usertoid', Input::post('usertoid', isset($reqfeed) ? $reqfeed->usertoid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Usertoid')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Message', 'message', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('message', Input::post('message', isset($reqfeed) ? $reqfeed->message : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Message')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>

				<?php echo Form::input('status', Input::post('status', isset($reqfeed) ? $reqfeed->status : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Status')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>