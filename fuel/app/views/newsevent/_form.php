<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Date', 'date', array('class'=>'control-label')); ?>

				<?php echo Form::input('date', Input::post('date', isset($newsevent) ? $newsevent->date : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Date')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Shortdescription', 'shortdescription', array('class'=>'control-label')); ?>

				<?php echo Form::input('shortdescription', Input::post('shortdescription', isset($newsevent) ? $newsevent->shortdescription : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Shortdescription')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('News', 'news', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('news', Input::post('news', isset($newsevent) ? $newsevent->news : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'News')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Image', 'image', array('class'=>'control-label')); ?>

				<?php echo Form::input('image', Input::post('image', isset($newsevent) ? $newsevent->image : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Image')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>

				<?php echo Form::input('status', Input::post('status', isset($newsevent) ? $newsevent->status : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Status')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Institudeid', 'institudeid', array('class'=>'control-label')); ?>

				<?php echo Form::input('institudeid', Input::post('institudeid', isset($newsevent) ? $newsevent->institudeid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Institudeid')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>