<h2>Listing <span class='muted'>Newsevents</span></h2>
<br>
<?php if ($newsevents): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Date</th>
			<th>Shortdescription</th>
			<th>News</th>
			<th>Image</th>
			<th>Status</th>
			<th>Institudeid</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($newsevents as $item): ?>		<tr>

			<td><?php echo $item->date; ?></td>
			<td><?php echo $item->shortdescription; ?></td>
			<td><?php echo $item->news; ?></td>
			<td><?php echo $item->image; ?></td>
			<td><?php echo $item->status; ?></td>
			<td><?php echo $item->institudeid; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('newsevent/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('newsevent/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('newsevent/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-small btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Newsevents.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('newsevent/create', 'Add new Newsevent', array('class' => 'btn btn-success')); ?>

</p>
