<h2>Editing <span class='muted'>Certificate</span></h2>
<br>

<?php echo render('certificate/_form'); ?>
<p>
	<?php echo Html::anchor('certificate/view/'.$certificate->id, 'View'); ?> |
	<?php echo Html::anchor('certificate', 'Back'); ?></p>
