<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Batch', 'batch', array('class'=>'control-label')); ?>

				<?php echo Form::input('batch', Input::post('batch', isset($certificate) ? $certificate->batch : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Batch')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Resultyear', 'resultyear', array('class'=>'control-label')); ?>

				<?php echo Form::input('resultyear', Input::post('resultyear', isset($certificate) ? $certificate->resultyear : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Resultyear')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Examyear', 'examyear', array('class'=>'control-label')); ?>

				<?php echo Form::input('examyear', Input::post('examyear', isset($certificate) ? $certificate->examyear : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Examyear')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Regid', 'regid', array('class'=>'control-label')); ?>

				<?php echo Form::input('regid', Input::post('regid', isset($certificate) ? $certificate->regid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Regid')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>

				<?php echo Form::input('status', Input::post('status', isset($certificate) ? $certificate->status : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Status')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>