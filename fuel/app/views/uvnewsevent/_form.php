<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Date', 'date', array('class'=>'control-label')); ?>

				<?php echo Form::input('date', Input::post('date', isset($uvnewsevent) ? $uvnewsevent->date : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Date')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Description', 'description', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('description', Input::post('description', isset($uvnewsevent) ? $uvnewsevent->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Description')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Uvid', 'uvid', array('class'=>'control-label')); ?>

				<?php echo Form::input('uvid', Input::post('uvid', isset($uvnewsevent) ? $uvnewsevent->uvid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Uvid')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>

				<?php echo Form::input('status', Input::post('status', isset($uvnewsevent) ? $uvnewsevent->status : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Status')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Batch', 'batch', array('class'=>'control-label')); ?>

				<?php echo Form::input('batch', Input::post('batch', isset($uvnewsevent) ? $uvnewsevent->batch : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Batch')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>