<h2>Listing <span class='muted'>Uvnewsevents</span></h2>
<br>
<?php if ($uvnewsevents): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Date</th>
			<th>Description</th>
			<th>Uvid</th>
			<th>Status</th>
			<th>Batch</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($uvnewsevents as $item): ?>		<tr>

			<td><?php echo $item->date; ?></td>
			<td><?php echo $item->description; ?></td>
			<td><?php echo $item->uvid; ?></td>
			<td><?php echo $item->status; ?></td>
			<td><?php echo $item->batch; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('uvnewsevent/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('uvnewsevent/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('uvnewsevent/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-small btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Uvnewsevents.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('uvnewsevent/create', 'Add new Uvnewsevent', array('class' => 'btn btn-success')); ?>

</p>
