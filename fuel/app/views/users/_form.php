<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Mobileno', 'mobileno', array('class'=>'control-label')); ?>

				<?php echo Form::input('mobileno', Input::post('mobileno', isset($user) ? $user->mobileno : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Mobileno')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Email', 'email', array('class'=>'control-label')); ?>

				<?php echo Form::input('email', Input::post('email', isset($user) ? $user->email : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Email')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Firstname', 'firstname', array('class'=>'control-label')); ?>

				<?php echo Form::input('firstname', Input::post('firstname', isset($user) ? $user->firstname : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Firstname')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Lastname', 'lastname', array('class'=>'control-label')); ?>

				<?php echo Form::input('lastname', Input::post('lastname', isset($user) ? $user->lastname : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Lastname')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Instituteid', 'instituteid', array('class'=>'control-label')); ?>

				<?php echo Form::input('instituteid', Input::post('instituteid', isset($user) ? $user->instituteid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Instituteid')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Regid', 'regid', array('class'=>'control-label')); ?>

				<?php echo Form::input('regid', Input::post('regid', isset($user) ? $user->regid : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Regid')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>