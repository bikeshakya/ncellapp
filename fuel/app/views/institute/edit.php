<h2>Editing <span class='muted'>Institute</span></h2>
<br>

<?php echo render('institute/_form'); ?>
<p>
	<?php echo Html::anchor('institute/view/'.$institute->id, 'View'); ?> |
	<?php echo Html::anchor('institute', 'Back'); ?></p>
