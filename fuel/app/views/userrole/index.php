<h2>Listing <span class='muted'>Userroles</span></h2>
<br>
<?php if ($userroles): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Rolename</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($userroles as $item): ?>		<tr>

			<td><?php echo $item->rolename; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('userrole/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('userrole/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-small')); ?>						<?php echo Html::anchor('userrole/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-small btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Userroles.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('userrole/create', 'Add new Userrole', array('class' => 'btn btn-success')); ?>

</p>
