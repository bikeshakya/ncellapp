<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Rolename', 'rolename', array('class'=>'control-label')); ?>

				<?php echo Form::input('rolename', Input::post('rolename', isset($userrole) ? $userrole->rolename : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Rolename')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>